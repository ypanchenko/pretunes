# -*- coding: utf-8 -*-

from playlist import Playlist
import requests
import logging
from vkapi import VKApi
import os
import urllib
from subler import Subler
import string
import sys


# Pretty iTunes
class PreTunes():

    def GetArtwork(self, artwork, folder):

        path = "%s.jpg" % folder
        artwork = artwork.replace(".100x100-75", ".1200x1200-75")
        urllib.urlretrieve(artwork, path)
        return path

    def Iter(self):
        sys.argv.pop(0)
        p = 0
        for band in sys.argv:
            band = sys.argv[p]
            print band

            # Убираем мусор
            os.system("rm -rf \"/Users/yegorpanchenko/Music/temp/*\"")

            # Запрашиваем результаты в itunes
            z = requests.get("https://itunes.apple.com/search?term=%s&entity=album&media=music" % band).json()

            if z['resultCount'] > 0:
                q = 0
                artid = z['results'][0]['artistId']
                albums = requests.get("http://itunes.apple.com/lookup?id=%s&entity=album" % artid).json()
                albums['results'].pop(0)
                for albumOf in albums['results']:

                    # Получаем первый альбом из списка

                    collectionId = albums['results'][q]['collectionId']
                    logging.info(u"Id коллекции - %s" % collectionId)

                    # Запрашиваем его
                    album = requests.get("http://itunes.apple.com/lookup?id=%s&entity=song" % collectionId).json()
                    collection = album["results"][0]

                    logging.info(u"Запрашиваю обложку")
                    albumArt = self.GetArtwork(album["results"][0]['artworkUrl100'], "/Users/yegorpanchenko/Music/temp/%s" % collectionId)

                    # Начинаем обход всех треков альбома
                    tracks = [s for s in album["results"] if s['wrapperType'] == "track"]

                    # Список треков для постинга на стену
                    attachments = []

                    # Загруженных файлов
                    downloads = 0

                    # Увеличиваем счетчик на 1 
                    q = q + 1
                    # Перебираем все треки альбома
                    for song in tracks:

                        # Короткое имя для поиска
                        shortName = "%s - %s" % (song['artistName'], song['trackName'])
                        logging.info(u"\n\n===========================\nОбрабатываю %s..." % shortName)

                        # Ищем трек вконтакте
                        URL, Artist, Title, AID, Owner = VKApi.obj.musicSearch(shortName)
                        attachments.append("audio%s_%s" % (Owner, AID))


                        if not URL:
                            logging.error(u"Не удалось найти вконтактике трек %s" % shortName)

                        else:

                            # Загружаем файл
                            logging.info(u"Загружаю...")
                            ret = os.system("wget %s -nc -P /Users/yegorpanchenko/Music/temp" % URL)
                            Path = "/Users/yegorpanchenko/Music/temp/%s" % URL.rsplit("/", 1)[1]

                            # Файл успешно загрузился!
                            if ret == 0:

                                # Конвертируем
                                attept = 0
                                convertComplete = False

                                # Дадим 3 попытки сконвертировать
                                while attept <= 3 and not convertComplete:

                                    logging.info(u"Конвертирую (попытка %s)..." % attept)
                                    ret = os.system("ffmpeg -y -i %s -acodec libfaac -aq 250 -ab 218k -ar 48000 -ac 2 %s.temp.m4a" % (Path, Path))

                                    if ret > 0:
                                        attept += 1
                                    else:
                                        convertComplete = True

                                # Файл сконвертировался
                                if ret == 0:

                                    # Получаем текст песни
                                    logging.info(u"Ищу текст песни...")

                                    # Оформляем файл
                                    obj = Subler()

                                    try:
                                        lyrics = requests.get("http://api.lyricfind.com/lyric.do?apikey=535e489ad312ab46bf9d9282c9ad4474&reqtype=default&output=json&trackid=artistname:%s,trackname:%s&useragent=%s" % (song['artistName'], song['trackName'], "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)")).json()
                                        obj.Lyrics = lyrics['track']['lyrics'].replace("\r\n", "               ").replace("\"", "'")
                                    except Exception as e:
                                        logging.warning(u"Проблема с поиском текста песни: %s" % e)

                                    obj.Source(Path + ".temp.m4a")
                                    obj.Dest(Path + ".m4a")
                                    obj.AddArtwork(albumArt)
                                    obj.Name = song['trackName']
                                    obj.Artist = song['artistName']
                                    obj.AlbumArtist = collection['artistName']
                                    obj.Album = collection['collectionName']
                                    obj.Grouping = collection['collectionName']
                                    obj.Composer = song['artistName']
                                    obj.Genre = song['primaryGenreName']
                                    obj.Copyright = collection['copyright']
                                    obj.EncodingTool = "PreTunes 1.0 (c) 2012, gleero.com"
                                    obj.EncodedBy = "(c) 2012, Gleero"
                                    obj.MediaKind = "Music"
                                    obj.ReleaseDate = song['releaseDate']
                                    obj.Track = "%s/%s" % (song['trackNumber'], collection['trackCount'])
                                    obj.Disk = "%s/%s" % (song['discNumber'], song['discCount'])
                                    obj.iTunesAccount = "vladimir@perekladov.ru"
                                    obj.ContentRatings = "Clear" if song['trackExplicitness'] == "notExplicit" else "Explicit"
                                    obj.Save()

                                    # Удаляем временные файлы
                                    os.remove("%s.temp.m4a" % Path)
                                    os.remove(Path)

                                    # Отправляем в правильную папку
                                    valid_chars = "-_() %s%s" % (string.ascii_letters, string.digits)
                                    print valid_chars
                                    saveTo = "/Users/yegorpanchenko/Music/%s/%s" % (
                                        ''.join(c for c in collection['artistName'] if c in valid_chars),
                                        ''.join(c for c in collection['collectionName'] if c in valid_chars)
                                    )
                                    print saveTo

                                    # Создаём, если ещё не создана папка
                                    try:
                                        os.makedirs(saveTo)
                                    except:
                                        pass

                                    # Переносим файл
                                    newfile = "%s/%s %s.m4a" % (
                                        saveTo,
                                        song['trackNumber'],
                                        ''.join(c for c in song['trackName'] if c in valid_chars))

                                    try:
                                        os.rename("%s.m4a" % Path, newfile)
                                        downloads += 1

                                    except:
                                        logging.error(u"Не могу перенести файл в библиотеку.")

                                else:

                                    logging.error(u"Не получилось сконвертировать файл %s" % Path)

                            else:

                                logging.error(u"Не получилось скачать вконтакте %s" % shortName)


                    # / Конец перебора треков


            else:

                logging.error(u"В айтюнсе ничего не нашлось :(")
            p = p + 1
